
def palindrome(input):
    if input == input[::-1]:
        return True
    return False


if __name__ == '__main__':

    name = "racecar"
    print(palindrome(name))
