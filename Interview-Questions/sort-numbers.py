# The problem is that we want to sort a T[] one-dimensional array
# of integers in O(N) running time - and without any extra memory.
# The array can contain values: 0, 1 and 2 (check out the theoretical section for further information).

def sort(n):

    if len(n) == 1:
        return n

    middle_index = len(n) // 2

    left_list = sort(n[:middle_index])
    right_list = sort(n[middle_index:])

    i = 0
    j = 0
    k = 0

    while i < len(left_list) and j < len(right_list):
        if left_list[i] < right_list[j]:
            n[k] = left_list[i]
            i += 1
            k += 1
        else:
            n[k] = right_list[j]
            j += 1
            k += 1

    while j < len(right_list):
        n[k] = right_list[j]
        j += 1
        k += 1

    while i < len(left_list):
        n[k] = left_list[i]
        i += 1
        k += 1

    return n


if __name__ =='__main__':
    n = [2, 0, 1, 1, 0, 2, 1, 0, 0, 2, 1, 2, 1]
    sort(n)
    print(n)

