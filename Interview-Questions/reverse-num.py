
# Our task is to design an efficient algorithm to reverse a given integer. 
# For example if the input of the algorithm is 1234 then the output should be 4321.

def reverse_num(n):
    reverse = 0
    while n > 0:
        # take least significant digit, store it in remainder
        remainder = n % 10
        # divide n by 10 to get rid of that digit
        n = n // 10
        # add that digit in it's correct spot
        reverse = reverse * 10 + remainder
    return reverse


if __name__ == '__main__':
    print(reverse_num(5321))
