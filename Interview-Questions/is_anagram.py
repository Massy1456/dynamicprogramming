
#  Construct an algorithm to check whether two words (or phrases) are anagrams or not!
# 
# "An anagram is a word or phrase formed by rearranging the letters of a different word or phrase, 
#  typically using all the original letters exactly once"
# 
# For example: restful and fluster
#

def is_anagram(s1, s2):
    j = 0
    i = 0
    while i <= len(s1) - 1:
        if j >= len(s1) - 1:
            return True

        if s1[i] == s2[j]:
            j += 1
            i = 0
            continue

        i += 1

    return False


if __name__ == '__main__':

    str1 = "abc"
    str2 = "bca"
    print(is_anagram(str1, str2))


