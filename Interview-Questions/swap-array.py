
def reverse(numbers):
    j = len(numbers) - 1
    for i in range(len(numbers)):
        numbers[i], numbers[j] = numbers[j], numbers[i]
        j -= 1
        if j <= i:
            break

    return numbers


if __name__ == '__main__':
    nums = [1, 2, 3, 4, 5]
    print(reverse(nums))
