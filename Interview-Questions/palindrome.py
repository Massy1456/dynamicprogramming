
def palindrome(input):
    j = len(input) - 1
    for i in range(len(input)):
        if input[i] != input[j]:
            return False
        else:
            j -= 1
    return True


if __name__ == '__main__':

    name = "racecar"
    print(palindrome(name))
