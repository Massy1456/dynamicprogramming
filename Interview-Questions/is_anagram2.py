
#  Construct an algorithm to check whether two words (or phrases) are anagrams or not!
#
# "An anagram is a word or phrase formed by rearranging the letters of a different word or phrase,
#  typically using all the original letters exactly once"
#
# For example: restful and fluster
#

def is_anagram(s1, s2):

    if len(s1) != len(s2):
        return False

    s1 = sorted(s1)
    s2 = sorted(s2)

    for i in range(len(s1)):
        if s1[i] == s2[i]:
            continue
        else:
            return False

    return True


if __name__ == '__main__':

    str1 = "abc"
    str2 = "bca"
    print(is_anagram(str1, str2))
