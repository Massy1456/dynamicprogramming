import random


class QuickSelect:

    def __init__(self, nums):
        self.nums = nums
        self.first_index = 0
        self.last_index = len(nums) - 1

    def run(self, k):
        return self.select(self.first_index, self.last_index, k-1)

    def swap(self, i, j):
        self.nums[i], self.nums[j] = self.nums[j], self.nums[i]

    def partition(self, first_index, last_index):
        # make a random index the pivot point
        pivot_index = random.randint(first_index, last_index)

        # swap the element in the pivot index with that of the last index
        self.swap(pivot_index, last_index)

        # if current element is less than that of the pivot,
        # swap current element with that of the first element
        for i in range(first_index, last_index):
            if self.nums[i] < self.nums[last_index]:
                self.swap(i, first_index)
                first_index += 1

        # swap the element in the first index with that of the pivot
        self.swap(first_index, last_index)

        # return the pivot index
        return first_index

    def select(self, first_index, last_index, k):

        pivot_index = self.partition(first_index, last_index)

        # selection phase
        if pivot_index < k:
            return self.select(pivot_index + 1, last_index, k)
        elif pivot_index > k:
            return self.select(first_index, pivot_index - 1, k)

        return self.nums[pivot_index]


x = [1, 2, -5, 10, 100, -7, 3, 4]
select = QuickSelect(x)
print(select.run(2))
