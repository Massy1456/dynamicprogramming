def fib(n):
    r2, r1 = 0, 1
    
    for i in range(n-1):
        temp = r1
        r1 = r1+r2
        r2 = temp

    return r1


print(fib(8))


def fib2(n):
    a, b = 0, 1

    while a < n:
        a, b = b, a+b


print(fib(8))
