import random


class QuickSelect:

    def __init__(self, nums):
        self.nums = nums
        self.first_index = 0
        self.last_index = len(nums) - 1

    def run(self, k):
        return self.quicksort(self.first_index, self.last_index, k - 1)

    def swap(self, i, j):
        self.nums[i], self.nums[j] = self.nums[j], self.nums[i]

    def partition(self, first_index, last_index):
        pivot = random.randint(first_index, last_index)

        self.swap(last_index, pivot)

        for i in range(first_index, last_index):
            if self.nums[i] < self.nums[last_index]:
                self.swap(i, first_index)
                first_index += 1

        self.swap(first_index, last_index)
        return first_index

    def quicksort(self, first_index, last_index, k):
        pivot_index = self.partition(first_index, last_index)

        if pivot_index < k:
            return self.quicksort(pivot_index + 1, last_index, k)
        elif pivot_index > k:
            return self.quicksort(first_index, pivot_index - 1, k)

        return self.nums[pivot_index]

    # sort by starting with an empty list
    def sort(self):
        sorted_list = []

        # because we decrement the k value (k'=k-1) this is why
        # we have to use range() like that
        for i in range(1, len(self.nums) + 1):
            sorted_list.append(self.run(i))

        return sorted_list


if __name__ == '__main__':

    x = [1, 2, -5, 10, 100, -7, 3, 4]
    select = QuickSelect(x)
    print(select.sort())
