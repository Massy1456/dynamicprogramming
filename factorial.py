def fact_head(n):
    if n == 1:
        return n
    return n * fact_head(n - 1)


print(fact_head(7))


def fact_tail(n, result):
    if n == 1:
        return result

    return fact_tail(n - 1, n * result)


print(fact_tail(7, 1))
