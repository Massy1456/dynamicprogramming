
class QueensProblem:

    def __init__(self, n):
        # number of queens
        self.n = n
        self.chess_table = [[0 for i in range(n)] for j in range(n)]

    # first queen will have index 0, second = 1, third = 2
    def solve_n_queens(self):
        if self.solve(0) == True:
            self.print_queens()
        else:
            print('No solution')

    # column index is the same as the queen index
    def solve(self, col_index):
        # base case - we have solved the problem
        # if the column index is equal to the number of queens, that means we have succeeded
        # because the number of queens starts at 0, so if that means we have finished the number
        # on rows in the chessboard
        if col_index == self.n:
            return True

        # try to find position for queen
        for row_index in range(self.n):
            if self.is_place_valid(row_index, col_index):
                # 1 means there is a queen at the given location
                self.chess_table[row_index][col_index] = 1

                # recursively call function with col + 1
                # try to find the location of the next queen
                if self.solve(col_index + 1) == True:
                    return True

                self.chess_table[row_index][col_index] = 0

        # when we have considered all thr rows in a col
        # without finding a valid cell for queen
        return False

    def is_place_valid(self, row_index, col_index):

        # check to see if theres already a queen in that row
        # if there is, return false since that row is not valid
        for i in range(self.n):
            if self.chess_table[row_index][i] == 1:
                return False

        # check the diagonals
        # top left to bottom right
        j = col_index
        for i in range(row_index, -1, -1):

            if i < 0:
                break

            if self.chess_table[i][j] == 1:
                return False

            j = j - 1

        # check diagonals
        # top right to bottom left
        j = col_index
        for i in range(row_index, self.n):

            if j < 0:
                break

            if self.chess_table[i][j] == 1:
                return False

            j = j - 1

        return True

    def print_queens(self):
        for i in range(self.n):
            for j in range(self.n):
                if self.chess_table[i][j] == 1:
                    print(' Q ', end='')
                else:
                    print(' - ', end='')
            print('\n')


if __name__ == '__main__':
    queens = QueensProblem(8)
    queens.solve_n_queens()
