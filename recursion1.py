def tail(n):
    if n == 1:
        return
    print(n)
    tail(n - 1)


tail(10)


def head(n):
    if n == 1:
        return
    head(n - 1)
    print(n)


head(10)
