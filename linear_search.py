# linear search both recursively and iteratively

def linear_1(array, n):
    for i in array:
        if n == i:
            print('item found')
            return
    print('item not found')
    return -1


def linear_2(container, item, itr):
    if container[itr] == item:
        print('item found')
        return
    if itr < 0:
        print('item not found')
        return
    linear_2(container, item, itr - 1)


arr = [5, 8, 16, 23, 1, 4, 6, 20]
linear_1(arr, 22)
linear_2(arr, 23, len(arr)-1)
