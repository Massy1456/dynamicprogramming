def hanoi(disk, source, middle, destination):

    # base case, 0 is the smallest disk
    if disk == 0:
        print('Disk %s moved from %s to %s' % (disk, source, destination))
        return

    hanoi(disk - 1, source, destination, middle)
    print('Disk %s moved from %s to %s' % (disk, source, destination))
    hanoi(disk-1, middle, source, destination)


hanoi(2, 'A', 'B', 'C')
