def binary_search(container, item, left, right):
    # base case 1
    if right < left:
        print('item not found')
        return -1

    # generate the index of the middle item
    middle_index = (left + right) // 2

    if container[middle_index] == item:
        print('item found!')
        return middle_index
    # the item is in the left sub array
    elif container[middle_index] > item:
        print('Checking items on the left...')
        binary_search(container, item, left, middle_index - 1)
    # the item is in the right sub array
    elif container[middle_index] < item:
        print('Checking items on the right...')
        binary_search(container, item, middle_index + 1, right)


nums = [2, 5, 8, 11, 18, 25, 28, 32, 36, 41]
binary_search(nums, 32, 0, len(nums) - 1)
