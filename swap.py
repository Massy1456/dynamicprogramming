# helpful to remember you can inline swap without the need of a temp variable in python

def swap(a,b):
    a, b = b, a
    return a, b


print(swap(8, 12))
