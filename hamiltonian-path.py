
class HamiltonianPath:

    def __init__(self, adjacency_matrix):
        self.n = len(adjacency_matrix)
        self.adjacency_matrix = adjacency_matrix
        self.path = [0]

    def hamiltonian_path(self):

        if self.solve(1):
            self.show_hamiltonian_path()
        else:
            print('No solution')

    def solve(self, position):
        if position == self.n:
            return True

        for vertex_index in range(1, self.n):
            if self.is_feasible(vertex_index, position):
                self.path.append(vertex_index)

                if self.solve(position + 1):
                    return True

                # if false, then pop the vertex from the path
                self.path.pop()

        # if we have considered all the vertexs without success
        return False

    def is_feasible(self, vertex_index, position):

        # if 0, that means there is no connection between the vertices
        if self.adjacency_matrix[self.path[position - 1]][vertex_index] == 0:
            return False

        #  check if we have already included the given vertex in the results
        for i in range(position):
            if self.path[i] == vertex_index:
                return False

        return True

    def show_hamiltonian_path(self):
        for v in self.path:
            print(v)


if __name__=='__main__':

    m = [[0, 1, 0, 0, 0, 1],
         [1, 0, 1, 0, 0, 0],
         [0, 1, 0, 0, 1, 0],
         [0, 0, 0, 0, 1, 1],
         [0, 0, 1, 1, 0, 1],
         [1, 0, 0, 1, 1, 0]]

    ham_path = HamiltonianPath(m)
    ham_path.hamiltonian_path()

