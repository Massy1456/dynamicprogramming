
class KnightsTour:
    def __init__(self, size_of_board):
        self.n = size_of_board
        self.solution_board = [[-1 for _ in range(self.n)] for _ in range(self.n)]
        # x and y moves, used in tandem (2, 1) or (-1, 2)
        # move 2 up and 1 right, move 1 down and 2 right
        self.x_moves = [2, 1, -1, -2, -2, -1, 1, 2]
        self.y_moves = [1, 2, 2, 1, -1, -2, -2, -1]

    def run(self):

        # start with the top left cell
        self.solution_board[0][0] = 0

        # first parameter is counter
        # second and third parameter is the location (x, y)
        if self.knight_tour(1, 0, 0):
            self.print()
        else:
            print('Not possible')

    def knight_tour(self, counter, x, y):

        # base case - if position is equal to the size of the chess board, return true
        if counter == self.n * self.n:
            return True

        for move_index in range(len(self.x_moves)):

            # the next x and y are equal to the current coordinates plus the index on the list
            next_x = x + self.x_moves[move_index]
            next_y = y + self.y_moves[move_index]

            if self.is_valid(next_x, next_y):
                self.solution_board[next_x][next_y] = counter

                # recursively call the function with next move
                if self.knight_tour(counter + 1, next_x, next_y):
                    return True

                # backtrack if solution isn't valid
                self.solution_board[next_x][next_y] = -1

        return False

    def is_valid(self, x, y):

        # if x-coordinate goes off the board, return false
        if x < 0 or x >= self.n:
            return False

        # if y-coordinate goes off the board, return false
        if y < 0 or y >= self.n:
            return False

        # if that chess board position has already been traversed, return false
        if self.solution_board[x][y] > -1:
            return False

        return True

    def print(self):
        for i in range(self.n):
            for j in range(self.n):
                print(" %s " % self.solution_board[i][j], end='')
            print('\n')


if __name__ == '__main__':
    tour = KnightsTour(8)
    tour.run()
