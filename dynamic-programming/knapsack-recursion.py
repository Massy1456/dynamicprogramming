
def knapsack(max_capacity, weight, value, item):

    # base case
    if item == 0 or max_capacity == 0:
        return 0
    
    # check to see if it fits in the bag
    if weight[item - 1] > max_capacity:
        return knapsack(max_capacity, weight, value, item - 1)
    else:
        include = value[item - 1] + knapsack(max_capacity - weight[item - 1], weight, value, item - 1)
        exclude = knapsack(max_capacity, weight, value, item - 1)

    return max(include, exclude)


if __name__ == '__main__':

    weight_list = [5, 2, 3]
    value_list = [10, 4, 7]
    print(knapsack(5, weight_list, value_list, 3))
