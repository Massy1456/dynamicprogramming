
class KnapsackProblem:

    def __init__(self, n, M, w, v):
        self.num_of_items = n  # number of items
        self.max_capacity = M  # Max Capacity
        self.weight = w  # weights of items
        self.value = v  # values of items

        # creates the matrix
        self.S = [[0 for _ in range(M+1)] for _ in range(n+1)]

    def solve(self):

        for index in range(self.num_of_items + 1):
            for weight_col in range(self.max_capacity + 1):
                # takes item right above it
                exclude_item = self.S[index - 1][weight_col]
                include_item = 0

                # if weight of item is less than remaining weight
                if self.weight[index] <= weight_col:
                    # item is equal to the value of that item + the value
                    include_item = self.value[index] + self.S[index - 1][weight_col - self.weight[index]]

                # memoization
                self.S[index][weight_col] = max(exclude_item, include_item)

    def show_result(self):
        print('Total Benefit: ', self.S[self.num_of_items][self.max_capacity])

        w = self.max_capacity
        for n in range(self.num_of_items, 0, -1):
            if self.S[n][w] != 0 and self.S[n][w] != self.S[n - 1][w]:
                print("We take item #%d" % n)
                w = w - self.weight[n]


if __name__ == '__main__':

    num_of_items = 4
    knapsack_capacity = 7
    weights = [0, 1, 3, 4, 5]
    values = [0, 1, 4, 5, 7]

    knapsack = KnapsackProblem(num_of_items, knapsack_capacity, weights, values)
    knapsack.solve()
    knapsack.show_result()
