class RodCutting:
    def __init__(self, prices, length_of_rod):
        self.prices = prices
        self.n = length_of_rod
        self.S = [[0 for _ in range(self.n + 1)] for _ in range(len(self.prices))]

    def solve(self):
        
        for index in range(1, len(self.prices)):
            for length in range(self.n + 1):
                if index <= length:
                    exclude = self.S[index - 1][length]
                    include = self.prices[index] + self.S[index][length - index]
                    self.S[index][length] = max(exclude, include)
                else:
                    self.S[index][length] = self.S[index - 1][length]

    def printout(self):
        print(self.S)
        print('Max val: ', self.S[len(self.prices) - 1][self.n])


if __name__ == '__main__':
    # price of each piece length
    p = [0, 2, 5, 7, 3, 9]
    # total length of rod
    length_of_rod = 5
    rod = RodCutting(p, length_of_rod)
    rod.solve()
    rod.printout()
