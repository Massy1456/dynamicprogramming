
class Subset:
    def __init__(self, A, m):
        self.numbers = A
        self.sum = m
        self.S = [[False for _ in range(self.sum + 1)] for _ in range(len(self.numbers) + 1)]

    def solve(self):

        # Make the first column of the table all True
        for row_index in range(len(self.numbers) + 1):
            self.S[row_index][0] = True

        # Construct the table with the cells one by one
        for row_index in range(1, len(self.numbers) + 1):
            for col_index in range(1, self.sum + 1):
                # if the current column is less than the integer in the array table
                if col_index < self.numbers[row_index - 1]:
                    self.S[row_index][col_index] = self.S[row_index - 1][col_index]
                else:
                    if self.S[row_index - 1][col_index]:
                        # if previous was True make this cell also True
                        self.S[row_index][col_index] = self.S[row_index - 1][col_index]
                    else:
                        # look at the position of 1 row up and go to the left the amount of spaces
                        # equal to the integer in the numbers
                        self.S[row_index][col_index] = self.S[row_index - 1][col_index - self.numbers[row_index - 1]]

    def printf(self):

        print('The problem is feasible: %s' % self.S[len(self.numbers)][self.sum])

        if not self.S[len(self.numbers)][self.sum]:
            return

        col_index = self.sum
        row_index = len(self.numbers)

        while col_index > 0 or row_index > 0:
            # if the lower row is True, simply go down another row until you find one that is False
            if self.S[row_index][col_index] == self.S[row_index - 1][col_index]:
                row_index -= 1
            else:
                # if the lower row is False
                print('We take item: %d' % self.numbers[row_index - 1])
                # subtract the numbers[] value from the columns
                col_index = col_index - self.numbers[row_index - 1]
                row_index -= 1


if __name__ == '__main__':

    arr = [5, 2, 1, 3]
    sum = 9
    sub = Subset(arr, sum)
    sub.solve()
    sub.printf()
