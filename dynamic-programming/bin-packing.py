
def first_fit(capacities, bin_max_capacity):

    # bins are created during run time
    solution_bins = []
    # [[item 1, item2][item 4][item 3, item 5, item 6]]

    item_names = list(capacities.keys())
    # sorts the items based on their integers
    # sorts item_names based on the value of key as applied to each element of the list
    sorted_items = sorted(item_names, key=lambda x: capacities[x], reverse=True)

    for item in sorted_items:
        bin_found = False
        item_capacity = capacities[item]

        # consider all the bins
        for index, actual_bin in enumerate(solution_bins):

            # sum of items capacity so far in the actual bin
            actual_bin_summed = sum([capacities[key] for key in actual_bin])

            # if true, we are able to include the current item into the bin!
            if item_capacity <= bin_max_capacity - actual_bin_summed:
                solution_bins[index].append(item)
                bin_found = True
                break

        # if there is no space for the item in the bins
        # we must create a new bin
        if not bin_found:
            solution_bins.append([item])

    return solution_bins



if __name__ == '__main__':

    item_table = {"item #1": 4, "item #2": 2, "item #3": 7, "item #4": 5, "item #5": 6, "item #6": 3}

    print(first_fit(item_table, 8))
