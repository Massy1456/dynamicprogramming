
def fib_memoization(n, table):

    if n not in table:
        table[n] = fib_memoization(n - 1, table) + fib_memoization(n - 2, table)

    return table[n]


def fib_tabulation(n, table):

    for i in range(2, n + 1):
        table[i] = table[i - 1] + table[i - 2]

    return table[n]


t = {0: 1, 1: 1}
print(fib_memoization(5, t))
print(fib_tabulation(10, t))
