
def median_algorithm(nums, k):

    # split the list into 5 chunks
    # goes from 0 to 4, then adds 5, then goes from 5 to 10, then adds 5
    chunks = [nums[i:i+5] for i in range(0, len(nums), 5)]

    # make an array 'medians' to hold all the medians from every chunk
    medians = [sorted(chunk)[len(chunk)//2] for chunk in chunks]
    # returns the value of the middle index
    pivot_value = sorted(medians)[len(medians)//2]

    # partition phase
    left_array = [n for n in nums if n < pivot_value]
    right_array = [m for m in nums if m > pivot_value]

    # pivot index will be in the middle, hence, at the end of the left array
    pivot_index = len(left_array)

    if k < pivot_index:
        return median_algorithm(left_array, k)
    elif k > pivot_index:
        return median_algorithm(right_array, k - len(left_array) - 1)
    else:
        return pivot_value


def select(nums, k):
    return median_algorithm(nums, k-1)


x = [1, -5, 6, 12, 7, 34, 2, 5, 8, 17]
print(select(x, 1))
