def merge_sort(nums):

    # base case
    # split the list until you have sub-lists with just 1 item
    if len(nums) == 1:
        return

    # DIVIDE PHASE
    middle_index = len(nums) // 2
    left_half = nums[:middle_index]
    right_half = nums[middle_index:]

    merge_sort(left_half)
    merge_sort(right_half)

    # CONQUER PHASE
    i = 0  # left sublist iterator
    j = 0  # right sublist iterator
    k = 0  # main list iterator
    while i < len(left_half) and j < len(right_half):
        if left_half[i] > right_half[j]:
            nums[k] = left_half[i]
            i += 1
        else:
            nums[k] = right_half[j]
            j += 1

        k += 1

    # if there are any additional items left in the right (left) list, add them to main list
    while i < len(left_half):
        nums[k] = left_half[i]
        i += 1
        k += 1

    while j < len(right_half):
        nums[k] = right_half[j]
        j += 1
        k += 1


if __name__ == '__main__':

    unsorted_numbers = [-5, 2, 6, 21, 11, 25, 18, -8, 12]
    merge_sort(unsorted_numbers)
    print(unsorted_numbers)
