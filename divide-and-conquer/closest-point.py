
import math


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


# Euclidean distance
def distance(p, q):
    return math.sqrt((p.x-q.x)*(p.x-q.x)+(p.y-q.y)*(p.y-q.y))


def brute_force(sub_array):

    min_distance = float('inf')

    # calculate the distance between every single point
    # and find the minimum distance

    for i in range(len(sub_array) - 1):
        for j in range(i+1, len(sub_array)):
            found_dist = distance(sub_array[i], sub_array[j])
            if found_dist < min_distance:
                min_distance = found_dist

    return min_distance


def get_strip_delta(strip_points, delta):

    # the minimum distance is the distance that was already calculated as the smallest
    # from the left and right sub-array
    min_distance = delta
    n = len(strip_points)

    for i in range(n):
        j = i + 1
        while j < n and (strip_points[j].y - strip_points[i].y) < min_distance:
            min_distance = distance(strip_points[j], strip_points[i])
            j += 1

    return min_distance


def closest_pairs_algorithm(list_sorted_x, list_sorted_y, num_of_items):

    # when the number of items is less than 3, we use brute-force
    if num_of_items <= 3:
        return brute_force(list_sorted_x)

    middle_index = num_of_items // 2
    middle_item = list_sorted_x[middle_index]

    delta_left = closest_pairs_algorithm(list_sorted_x[:middle_index], list_sorted_y, middle_index)
    delta_right = closest_pairs_algorithm(list_sorted_x[middle_index:], list_sorted_y, num_of_items - middle_index)

    delta = min(delta_left, delta_right)

    strip_points = []

    for i in range(num_of_items):
        if abs(list_sorted_y[i].x - middle_item.x) < delta:
            strip_points.append(list_sorted_y[i])

    strip_delta = get_strip_delta(strip_points, delta)

    return min(strip_delta, delta)


def run(list1, list2):
    list1.sort(key=lambda point: point.x)
    list2.sort(key=lambda point: point.y)
    return closest_pairs_algorithm(list1, list2, len(list1))


if __name__ == '__main__':

    points = [Point(1, 1), Point(2, 4), Point(3, 6), Point(1, 5), Point(2, 7), Point(3, 2)]

    l1 = list(points)
    l2 = list(points)
    print(run(l1, l2))
