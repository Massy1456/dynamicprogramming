
class ColorProblem:
    def __init__(self, adjacency_matrix, number_of_colors):
        self.adj_matrix = adjacency_matrix
        self.n = len(adjacency_matrix)
        self.num_of_colors = number_of_colors
        self.colors = [0 for _ in range(self.n)]

    def run(self):
        if self.solve(0):
            self.print_results()
        else:
            print('Cannot be solved')

    def solve(self, index):
        
        # once the index is equal to the number of vertices, return
        if index == self.n:
            return True
        
        # looks for a valid color for that index
        # if it finds one, it goes to the next index via recursion
        for color in range(1, self.num_of_colors + 1):
            if self.is_feasible(index, color):
                self.colors[index] = color

                if self.solve(index + 1):
                    return True

        return False

    def is_feasible(self, index, color):
        
        # check to see if the two vertices are adjacent
        # and also check if they share the same color. If they do, return false 
        # so that the program can try the next color.
        for i in range(self.n):
            if self.adj_matrix[index][i] == 1 and color == self.colors[i]:
                return False
        return True

    def print_results(self):
        print(self.colors)


if __name__ == '__main__':
    adj_matrix = [[0, 1, 1, 1, 0, 0],
                  [1, 0, 1, 0, 1, 1],
                  [1, 1, 0, 1, 0, 1],
                  [1, 0, 1, 0, 0, 1],
                  [0, 1, 0, 0, 0, 1],
                  [0, 1, 1, 1, 1, 0],
                  ]
    num_of_colors = 3
    color_problem = ColorProblem(adj_matrix, num_of_colors)
    color_problem.run()
