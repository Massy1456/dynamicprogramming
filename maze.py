
from collections import deque

class Maze:

    def __init__(self, matrix):
        self.map = matrix
        self.visited = [[False for _ in range(len(self.map))] for _ in range(len(self.map))]
        self.x_input = [1, 0, -1, 0]
        self.y_input = [0, 1, 0, -1]
        self.dist = 0

    def solve(self, x, y, final_x, final_y):

        self.visited[x][y] = True
        queue = deque()
        queue.append((x, y, self.dist))

        while queue:

            (x, y, self.dist) = queue.popleft()

            if x == final_x and y == final_y:
                break

            for index in range(len(self.x_input)):
                new_x = x + self.x_input[index]
                new_y = y + self.y_input[index]

                if self.test_pos(new_x, new_y):
                    queue.append((new_x, new_y, self.dist + 1))
                    self.visited[x][y] = True

    def test_pos(self, x, y):

        if x < 0 or x >= len(self.map):
            return False

        if y < 0 or y >= len(self.map):
            return False

        if self.map[x][y] == 0:
            return False

        if self.visited[x][y]:
            return False

        return True

    def print_dist(self):
        if self.dist > 1:
            print('The distance to the end of the maze is: ', self.dist)
        else:
            print('No possible solution')


if __name__ == "__main__":
    map = [[1, 1, 1, 0, 1],
           [0, 0, 1, 0, 1],
           [1, 1, 1, 1, 1],
           [1, 0, 0, 0, 0],
           [1, 1, 1, 1, 1]]
    maze = Maze(map)
    maze.solve(0, 0, 4, 4)
    maze.print_dist()
